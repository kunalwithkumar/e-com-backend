package com.wipro.ecom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EComApplication {

	public static void main(final String[] args) {

		SpringApplication.run(EComApplication.class, args);

	}

}
