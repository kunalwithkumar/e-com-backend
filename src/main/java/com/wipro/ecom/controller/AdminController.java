package com.wipro.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.ecom.dto.LoginDTO;
import com.wipro.ecom.entity.Admin;
import com.wipro.ecom.entity.Product;
import com.wipro.ecom.service.AdminService;

@RestController
@RequestMapping("/admin")
@CrossOrigin(value = "http://localhost:4200/")
public class AdminController {

	@Autowired
	private AdminService adminService;

	@PostMapping("/register")
	public ResponseEntity<Admin> register(@RequestBody final Admin admin) {
		return adminService.register(admin);
	}

	@PostMapping("/login")
	public ResponseEntity<Admin> login(@RequestBody final LoginDTO loginDTO) {
		return adminService.login(loginDTO);
	}

	@GetMapping("/logout/{adminId}")
	public ResponseEntity<Admin> logout(@PathVariable final Long adminId) {
		return adminService.logout(adminId);
	}

	@PostMapping("/addProduct")
	public ResponseEntity<List<Product>> addProduct(@RequestBody final Product product) {
		return adminService.addProduct(product);
	}

	@PutMapping("/updateProduct")
	public ResponseEntity<List<Product>> updateProduct(@RequestBody final Product product) {
		return adminService.updateProduct(product);
	}

	@DeleteMapping("/removeProduct/{productId}")
	public ResponseEntity<String> removeProduct(@PathVariable final Long productId) {
		return adminService.removeProduct(productId);
	}

	@DeleteMapping("/removeUser/{userId}")
	public ResponseEntity<String> removeUser(@PathVariable final Long userId) {
		return adminService.removeUser(userId);
	}

}
