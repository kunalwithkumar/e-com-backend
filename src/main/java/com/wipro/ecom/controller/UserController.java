package com.wipro.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.ecom.dto.LoginDTO;
import com.wipro.ecom.entity.Cart;
import com.wipro.ecom.entity.Product;
import com.wipro.ecom.entity.User;
import com.wipro.ecom.entity.WishList;
import com.wipro.ecom.service.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/register")
	public ResponseEntity<User> register(@RequestBody final User user) {
		return userService.register(user);
	}

	@GetMapping("/login")
	public ResponseEntity<User> login(@RequestBody final LoginDTO loginDTO) {
		return userService.login(loginDTO);
	}

	@GetMapping("/logout/{id}")
	public ResponseEntity<String> logout(@PathVariable final Long id) {
		return userService.logout(id);
	}

	@PutMapping("/update")
	public ResponseEntity<User> update(@RequestBody final User user) {
		return userService.update(user);
	}

	@GetMapping("/getProducts")
	public ResponseEntity<List<Product>> getProducts() {
		return userService.getProducts();
	}

	@PostMapping("/addToCart")
	public ResponseEntity<List<Cart>> addToCart(@RequestBody final Cart cart) {
		return userService.addToCart(cart);
	}

	@PostMapping("/addToWishList")
	public ResponseEntity<List<WishList>> addToWishList(@RequestBody final WishList wishList) {
		return userService.addToWishList(wishList);
	}

	@GetMapping("/getCart/{userId}")
	public ResponseEntity<List<Cart>> getCart(@PathVariable final Long userId) {
		return userService.getCart(userId);
	}

	@GetMapping("/getWishList/{userId}")
	public ResponseEntity<List<WishList>> getWishList(@PathVariable final Long userId) {
		return userService.getWishList(userId);
	}

	@DeleteMapping("/removeFromCart")
	public ResponseEntity<String> removeFromCart(@RequestBody final Cart cart) {
		return userService.removeFromCart(cart);
	}

	@DeleteMapping("/removeFromWishList")
	public ResponseEntity<String> removeFromWishList(@RequestBody final WishList wishList) {
		return userService.removeFromWishList(wishList);
	}

}
