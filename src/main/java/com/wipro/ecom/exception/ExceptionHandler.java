package com.wipro.ecom.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ExceptionHandler extends RuntimeException {

	private static final long serialVersionUID = 1L;

	@org.springframework.web.bind.annotation.ExceptionHandler(value = ProductNotFound.class)
	public ResponseEntity<Object> productNotFound(final ProductNotFound productNotFound) {
		return new ResponseEntity<>("Invalid ID ---> Product Not found", HttpStatus.NOT_FOUND);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = UserNotFound.class)
	public ResponseEntity<Object> userNotFound(final UserNotFound userNotFound) {
		return new ResponseEntity<>("Invalid ID ---> User Not found", HttpStatus.NOT_FOUND);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = CartNotFound.class)
	public ResponseEntity<Object> cartNotFound(final CartNotFound cartNotFound) {
		return new ResponseEntity<>("Invalid ID ---> Cart Not found", HttpStatus.NOT_FOUND);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = WishListNotFound.class)
	public ResponseEntity<Object> wishListNotFound(final WishListNotFound wishListNotFound) {
		return new ResponseEntity<>("Invalid ID ---> WishList Not found", HttpStatus.NOT_FOUND);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = ProductAlreadyPresent.class)
	public ResponseEntity<Object> productAlreadyPresent(final ProductAlreadyPresent productAlreadyPresent) {
		return new ResponseEntity<>("---> Product Already Present", HttpStatus.ALREADY_REPORTED);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = UserAlreadyPresent.class)
	public ResponseEntity<Object> userAlreadyPresent(final UserAlreadyPresent userAlreadyPresent) {
		return new ResponseEntity<>("---> User Already Present", HttpStatus.ALREADY_REPORTED);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = CartAlreadyThere.class)
	public ResponseEntity<Object> cartAlreadyThere(final CartAlreadyThere cartAlreadyThere) {
		return new ResponseEntity<>("---> Cart Already Present", HttpStatus.ALREADY_REPORTED);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = WishListAlreadyThere.class)
	public ResponseEntity<Object> wishListAlreadyThere(final WishListAlreadyThere wishListAlreadyThere) {
		return new ResponseEntity<>("---> WishList Already Present", HttpStatus.ALREADY_REPORTED);
	}
}
