package com.wipro.ecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.ecom.entity.Admin;

@Repository
public interface AdminRepo extends JpaRepository<Admin, Long> {

	Admin findByUserName(String userName);

	Admin findByEmail(String email);

}
