package com.wipro.ecom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.ecom.entity.Cart;

@Repository
public interface CartRepo extends JpaRepository<Cart, Long> {

	List<Cart> findByUserId(Long userId);

}
