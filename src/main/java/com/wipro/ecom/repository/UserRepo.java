package com.wipro.ecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.ecom.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

	User findByUserName(String userName);

	User findByEmail(String email);

}
