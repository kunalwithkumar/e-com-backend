package com.wipro.ecom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.ecom.entity.WishList;

@Repository
public interface WishListRepo extends JpaRepository<WishList, Long> {

	List<WishList> findByUserId(Long userId);

}
