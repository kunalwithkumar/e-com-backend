package com.wipro.ecom.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.wipro.ecom.dto.LoginDTO;
import com.wipro.ecom.entity.Admin;
import com.wipro.ecom.entity.Product;

public interface AdminService {

	ResponseEntity<Admin> register(Admin admin);

	ResponseEntity<Admin> login(LoginDTO loginDTO);

	ResponseEntity<Admin> logout(Long adminId);

	ResponseEntity<List<Product>> addProduct(Product product);

	ResponseEntity<List<Product>> updateProduct(Product product);

	ResponseEntity<String> removeProduct(Long productId);

	ResponseEntity<String> removeUser(Long userId);

}
