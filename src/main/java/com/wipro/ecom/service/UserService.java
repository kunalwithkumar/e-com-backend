package com.wipro.ecom.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.wipro.ecom.dto.LoginDTO;
import com.wipro.ecom.entity.Cart;
import com.wipro.ecom.entity.Product;
import com.wipro.ecom.entity.User;
import com.wipro.ecom.entity.WishList;

public interface UserService {

	public ResponseEntity< User> register(User user);

	public ResponseEntity<User> login(LoginDTO loginDTO);

	public ResponseEntity<String> logout(Long id);

	public ResponseEntity<User> update(User user);

	public ResponseEntity<List<Product>> getProducts();

	public ResponseEntity<List<Cart>> addToCart(Cart cart);

	public ResponseEntity<List<WishList>> addToWishList(WishList wishList);

	public ResponseEntity<List<Cart>> getCart(Long userId);

	public ResponseEntity<List<WishList>> getWishList(Long userId);

	public ResponseEntity<String> removeFromCart(Cart cart);

	public ResponseEntity<String> removeFromWishList(WishList wishList);

}
