package com.wipro.ecom.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wipro.ecom.dto.LoginDTO;
import com.wipro.ecom.entity.Admin;
import com.wipro.ecom.entity.Product;
import com.wipro.ecom.entity.User;
import com.wipro.ecom.exception.ProductNotFound;
import com.wipro.ecom.exception.UserAlreadyPresent;
import com.wipro.ecom.exception.UserNotFound;
import com.wipro.ecom.repository.AdminRepo;
import com.wipro.ecom.repository.ProductRepo;
import com.wipro.ecom.repository.UserRepo;
import com.wipro.ecom.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	private final ProductRepo productRepo;
	private final AdminRepo adminRepo;
	private final UserRepo userRepo;

	@Autowired
	public AdminServiceImpl(final ProductRepo productRepo, final AdminRepo adminRepo, final UserRepo userRepo) {
		this.adminRepo = adminRepo;
		this.productRepo = productRepo;
		this.userRepo = userRepo;
	}

	@Override
	public ResponseEntity<Admin> register(final Admin admin) {

		Admin admin2 = adminRepo.findByEmail(admin.getEmail());
		if (Objects.isNull(admin2)) {
			admin2 = adminRepo.save(admin);
			return new ResponseEntity<>(admin2, HttpStatus.CREATED);

		}
		throw new UserAlreadyPresent();
	}

	@Override
	public ResponseEntity<Admin> login(final LoginDTO loginDTO) {
		final Admin admin = adminRepo.findByUserName(loginDTO.getUserName());

		if (!Objects.isNull(admin))
			if (loginDTO.getPassword().equals(admin.getPassword()))
				return new ResponseEntity<>(admin, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
	}

	@Override
	public ResponseEntity<Admin> logout(final Long adminId) {
		return null;
	}

	@Override
	public ResponseEntity<List<Product>> addProduct(final Product product) {
		productRepo.save(product);
		final List<Product> products = productRepo.findAll();
		return new ResponseEntity<>(products, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Product>> updateProduct(final Product product) {
		productRepo.save(product);

		final List<Product> products = productRepo.findAll();
		return new ResponseEntity<>(products, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> removeProduct(final Long productId) {
		final Product product = productRepo.findById(productId).orElseThrow(ProductNotFound::new);
		if (!Objects.isNull(product)) {
			productRepo.delete(product);

			return new ResponseEntity<>("Deleted", HttpStatus.OK);
		}
		return new ResponseEntity<>("Error", HttpStatus.NOT_FOUND);

	}

	@Override
	public ResponseEntity<String> removeUser(final Long userId) {
		final User user = userRepo.findById(userId).orElseThrow(UserNotFound::new);
		if (!Objects.isNull(user)) {
			userRepo.delete(user);
			return new ResponseEntity<>("user deleted", HttpStatus.OK);
		}
		return new ResponseEntity<>("error", HttpStatus.NOT_FOUND);
	}

}
