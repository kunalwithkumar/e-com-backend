package com.wipro.ecom.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wipro.ecom.dto.LoginDTO;
import com.wipro.ecom.entity.Cart;
import com.wipro.ecom.entity.Product;
import com.wipro.ecom.entity.User;
import com.wipro.ecom.entity.WishList;
import com.wipro.ecom.exception.UserAlreadyPresent;
import com.wipro.ecom.exception.UserNotFound;
import com.wipro.ecom.repository.CartRepo;
import com.wipro.ecom.repository.ProductRepo;
import com.wipro.ecom.repository.UserRepo;
import com.wipro.ecom.repository.WishListRepo;
import com.wipro.ecom.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private CartRepo cartRepo;
	@Autowired
	private WishListRepo wishListRepo;

	@Autowired
	private ProductRepo productRepo;

	@Override
	public ResponseEntity<User> register(final User user) {
		User user2 = userRepo.findByEmail(user.getEmail());
		if (Objects.isNull(user2)) {
			user2 = userRepo.save(user);
			return new ResponseEntity<>(user2, HttpStatus.OK);
		}
		throw new UserAlreadyPresent();

	}

	@Override
	public ResponseEntity<User> login(final LoginDTO loginDTO) {

		final User user = userRepo.findByUserName(loginDTO.getUserName());
		if (!Objects.isNull(user))
			if (user.getPasssword().equals(loginDTO.getPassword()))
				return new ResponseEntity<>(user, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
	}

	@Override
	public ResponseEntity<String> logout(final Long id) {
		final User user = userRepo.findById(id).orElseThrow(UserNotFound::new);
		if (!Objects.isNull(user))
			return new ResponseEntity<>("Logged Out", HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
	}

	@Override
	public ResponseEntity<User> update(User user) {

		user = userRepo.findById(user.getId()).orElseThrow(UserNotFound::new);

		userRepo.save(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Product>> getProducts() {

		final List<Product> products = productRepo.findAll();
		return new ResponseEntity<>(products, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Cart>> addToCart(final Cart cart) {

		cartRepo.save(cart);

		final List<Cart> carts = cartRepo.findAll();

		return new ResponseEntity<>(carts, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<WishList>> addToWishList(final WishList wishList) {

		wishListRepo.save(wishList);

		final List<WishList> wishLists = wishListRepo.findAll();
		return new ResponseEntity<>(wishLists, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Cart>> getCart(final Long userId) {

		final List<Cart> carts = cartRepo.findByUserId(userId);

		return new ResponseEntity<>(carts, HttpStatus.OK);

	}

	@Override
	public ResponseEntity<List<WishList>> getWishList(final Long userId) {

		final List<WishList> wishLists = wishListRepo.findByUserId(userId);
		return new ResponseEntity<>(wishLists, HttpStatus.OK);

	}

	@Override
	public ResponseEntity<String> removeFromCart(final Cart cart) {

		cartRepo.delete(cart);

		return new ResponseEntity<>("Deleted", HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> removeFromWishList(final WishList wishList) {

		wishListRepo.delete(wishList);
		return new ResponseEntity<>("Deleted", HttpStatus.OK);
	}

}
